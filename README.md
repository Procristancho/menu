# EXPLICACIÓN DEL CÓDIGO  

En la línea número 1, encontramos el **encoding** de Python, esto no es mas que una directiva que se coloca al inicio de un archivo Python, con el fin de indicar al sistema,
la codificación de caracteres utilizada en el archivo.

De no hacerse lo anterior producirá un error de sitaxis: `SyntaxError: Non-ASCII Character[...]`

La línea número 2, la explicaré mas adelante.


## FUNCIÓN FACTORIAL (LÍNEA 4)
Esta función espera recibir un valor de tipo entero a través del método `input()`, este valor se guarda en la variable
**factor**, en la línea siguiente se crea la variable **number** esta recibe el valor digitado por el usuario y este valor se mantiene hasta el final de la ejecución, 
a continuación se crea la variable **acom** y la inicializamos con el valor **1**.

### Ciclo While (Línea 10)
En este ciclo condicionamos que la variable **factor** sea mayor que cero, ya que si es igual su resultado siempre seria cero. Seguido en la línea 11 la variable **acom**
se multiplica por la variable **factor** y ese producto se guarda en la variable **acom**, posteriormente en la línea 12 decrementamos en 1 la variable **factor**. Por útimo
se imprime el resultado del factorial.

## FUNCIÓN SUMA SUCESIVA (sum_sucesiva, línea 16)
Esta función espera recibir un número entero, con la ecuación `n(n+1)/2` se devuelve la suma sucesiva desde el numero 1 hasta n, siendo n el valor digitado por el usuario

## FUNCION BINARIO (binario, Línea 23)
Esta función espera recibir un número entero para convertirlo al sistema binario. En la línea 25 recibe el número del usuario, en la línea 26 se crea la variable **number**
que recibe el valor digitado por el usuario y este valor se mantiene hasta el final de la ejecución, seguido se crea en la línea 27 la variable **binary** de tipo string.

### Ciclo while (línea 29)
Se crea una condición donde la variable **num** al ser dividida entre dos no debe ser cero. En la línea siguiente encontramos la variable **binary** que recibe como valor el modulo de 
la division entre la variable **num** y el número 2 todo esto se convierte a String `str(num % 2)` y luego la concatemos a la misma variable **binary** así `str(num % 2) + binary`, por 
último la variable **num** se le asigna el valor de **num**  entre 2.

Fuera del ciclo while encontramos la variable **result** que tiene como valor el ultimo cociente de la variable **num** convertido en string de la siguiente manera: `str(num)` y 
concatenado a la variable **binary**. Para finalizar se imprime el resultado.

# EJECUCIÓN DEL PROGRAMA

Bien para iniciar en la línea 38, aparece el muy conocido `if __name__ = '__main__'`el cual es muy largo de explicar, por eso da click [aquí](https://es.stackoverflow.com/questions/32165/qu%C3%A9-es-if-name-main), 
la primer respuesta es excelente (a mi parecer !!claro).

En la línea 39 se crea la variable **end_up** con un valor booleano **False**, seguido en la linea 40 se crea una variable **message** que es la encargada de anunciar al usuario
que es lo desea hacer.

En la línea 44 creamos nuestro menu utilizando la clase OrderedDict. OrderedDict resibe una lista de tuplas que contienen dos valores, una clave y un valor; para nuestro ejemplo
recibe las llaves son **a , b, c** y el valor serian las funciones creadas anteriormente. Si dejaramos el menu tal como la línea 42, podría no salir en el orden deseado, recordemos
que los diccionarios hacen busquedas mediante llaves y no mediante índices.

Ahora bien, en la linea se crea el ciclo `while not end_up`, la palabra **not** esta negando la variable cuyo valor es **False**, lo que quiere decir, si **no** es "False" es "True", al ser verdadero
ejecuta su contenido.

Las líneas 54, 55 y 56 no las explico, creo que para todos es obvio.

En la línea 58 se crea un ciclo `for` en el cual se recorre nuestro menu a través de la función `iteritems()`; en la línea 59 creamos la variable **choice** que contiene el reccorrido del menu
con las variables `option` y `function`, debemos notar la expresión `function.__doc__` esta hace referencia a los comentarios en las líneas 5, 17 y 24, eso quiere decir que no usará el 
nombre de la función sino, el comentario para situarlo en el menu, por último en la línea 60 se imprime la variable **choice**

En la línea 62 se crea la variable **value_answer** que recibe un string por parte del usuario, como el usuario puede escribir en mayúscula, se utiliza el metodo lower() que se encarga de mantener 
todo lo que escriba el usuario como minúscula.

En la línea 63, la variable **end_up** sigue siendo **False** a no ser que el usuario escriba **terminar**, si eso pasara la variable cambia a **True** y al ser negada en el ciclo
`while not end_up` termina siendo **False** y saldriamos del ciclo imprimiendo la línea 70.

Ya finalizando en la línea 65 se crea la variable **method** que optiene el valor de la llave que le corresponde a **value_answer**, en caso de que la llave exista retornará su valor, de lo contrario 
devuelve **None**.

En la línea 66 creamos una condicional donde validamos que el valor de **method** sea diferente de **None**, si **method** es diferente de **None** entonces ejecutamos el método o la función 
en la línea 67.
